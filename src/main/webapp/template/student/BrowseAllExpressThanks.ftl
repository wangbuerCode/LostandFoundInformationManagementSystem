<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">感谢墙</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<table class="ui inverted red table">
				<thead>
				<tr class="center aligned">
					<th colspan="1">留言感谢墙</th>

				</tr>
				</thead>
				<tbody>
				<#list expressthanks as expressthank>
				<tr class="center aligned">
					<td>
<#--						<div class="ui items">-->
<#--							<div class="item">-->
<#--								<a class="ui tiny image">-->
<#--									<img src="${base}/template/image/11.jpg">-->
<#--								</a>-->
<#--								<div class="content">-->
<#--									<a class="header">${expressthank.title}</a>-->
<#--									<div class="description">-->
<#--										<${expressthank.content}-->
<#--									</div>-->
<#--								</div>-->
<#--							</div>-->
<#--						</div>-->
						<div class="ui card fluid">
							<div class="content">
								<div class="header">${expressthank.title}</div>
								<div class="meta">${expressthank.leaveTime}</div>
								<div class="description">
									<p>${expressthank.content}</p>
								</div>
							</div>
							<div class="extra content">
								<i class="check icon"></i>
								学号：${expressthank.stuId}
							</div>
						</div>
					</td>
				</tr>
                </#list>
				</tbody>
			</table>
		</div>
		<div class="three wide column"></div>
	</div>

</@override>
<@extends name="student_layout.ftl"></@extends>