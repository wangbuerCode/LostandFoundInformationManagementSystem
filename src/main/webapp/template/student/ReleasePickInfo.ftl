<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">发布信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<form class="ui form">
				<table class="ui celled striped table fluid">
					<tbody>
					<div class="ui hidden divider"></div>
					<tr>
						<td class="two wide  center aligned ">
							物品名字
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="text" name="thingsName">
							</div>

						</td>
						<td class="two wide  center aligned ">
							物品类型
						</td>
						<td class="three wide  center aligned ">
							<select class="ui dropdown" name="thingsType">
								<option value="" >选择类型</option>
                                <#list types as type>
									<option value="${type.id}">${type.type}</option>
                                </#list>
							</select>
						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							捡到地点
						</td>
						<td class="three wide  center aligned ">

							<div class="field">
								<input type="text" name="pickPlace" >
							</div>

						</td>
						<td class="two wide  center aligned ">
							捡到时间
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="date" name="pickTime" >
							</div>

						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							保存位置
						</td>
						<td class="three wide  center aligned ">

							<div class="field">
								<input type="text" name="storagePlace" >
							</div>

						</td>
						<td class="two wide  center aligned ">
							招领状态
						</td>
						<td class="three wide  center aligned ">
							<div class="field disabled">
								<input type="text" name="Status" value="0" >
							</div>

						</td>
					</tr>

					<tr>

						<td class="two wide  center aligned ">
							物品细节描述
						</td>
						<td class="three wide  center aligned " colspan="3">
							<div class="field">
								<input type="text" name="thingsDetail" >
							</div>
						</td>
					</tr>
					<tr>
						<td>

						</td>
						<td colspan="2">
							<div class="ui error message"></div>
						</td>
						<td>
							<div class="ui fluid large teal submit button ">发布</div>
						</td>
					</tr>

					</tbody>
				</table>
			</form>
		</div>
		<div class="three wide column"></div>
	</div>

	<script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                thingsName: {
                    rules: [{
                        type: 'empty',
                        prompt: 'thingsName不能为空'
                    }]
                },
                thingsDetail: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: '描述不能为空'
                        }]
                },
                pickPlace: {
                    rules: [{
                        type: 'empty',
                        prompt: '捡到地点不能为空'
                    }]
                },
                thingsType: {
                    rules: [{
                        type: 'empty',
                        prompt: 'type不能为空'
                    }]
                },
                pickTime: {
                    rules: [{
                        type: 'empty',
                        prompt: '捡到时间不能为空'
                    }]
                }
                ,
                storagePlace: {
                    rules: [{
                        type: 'empty',
                        prompt: 'storagePlace不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/student/SavePickInfo',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/student/BrowseMyPickInfo'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

	</script>
</@override>
<@extends name="student_layout.ftl"></@extends>