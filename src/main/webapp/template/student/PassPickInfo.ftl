<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">申请信息</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
            <#if ! application.isEmpty() >
				<table class="ui celled striped table fluid">

					<thead>
					<tr>
						<th class="center aligned">申请序号</th>
						<th class="center aligned">申请者名字</th>
						<th class="center aligned">申请者学号</th>
						<th class="center aligned">申请时间</th>
						<th class="center aligned">联系电话</th>

					</tr>
					</thead>
					<tbody>
                    <#list application as application>
						<tr>
							<td class="center aligned">${application.id}</td>
							<td class="center aligned">${application.stuName}</td>
							<td class="center aligned">${application.stuId}</td>
							<td class="center aligned">${application.applyTime}</td>
							<td class="center aligned">${application.tel}</td>

						</tr>
                    </#list>
					</tbody>
				</table>
            <#else >
				<div class="ui negative message">
					<i class="close icon"></i>
					<div class="header">
						没有信息！！！
					</div>
				</div>
            </#if>
		</div>
		<div class=" three wide column">
		</div>
	</div>

</@override>
<@extends name="student_layout.ftl"></@extends>