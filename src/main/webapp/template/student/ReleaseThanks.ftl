<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">发布感谢</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<form class="ui form">
				<table class="ui celled striped table fluid">
					<tbody>
					<div class="ui hidden divider"></div>
					<tr>
						<td class="two wide  center aligned ">
							标题
						</td>
						<td class="eight  wide  center aligned ">
							<div class="field">
								<input type="text" name="title">
							</div>

						</td>

					</tr>
					<tr>
						<td class="two wide  center aligned ">
							内容
						</td>
						<td class="seven wide  center aligned ">

							<div class="field">
								<textarea name="content"></textarea>
							</div>

						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							<div class="ui error message"></div>
						</td>
						<td class="seven wide  center aligned ">


							<div class="ui fluid large teal submit button ">发布</div>


						</td>
					</tr>

					</tbody>
				</table>
			</form>
		</div>
		<div class="three wide column"></div>
	</div>

	<script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                title: {
                    rules: [{
                        type: 'empty',
                        prompt: 'title不为空'
                    }]
                },
                content: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: '内容不能为空'
                        }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/student/SaveThanks',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/student/BrowseAllExpressThanks'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

	</script>
</@override>
<@extends name="student_layout.ftl"></@extends>