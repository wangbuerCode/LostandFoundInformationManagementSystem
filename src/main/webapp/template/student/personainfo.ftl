<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">个人信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<form class="ui form">
				<table class="ui celled striped table fluid">
					<tbody>
					<div class="ui hidden divider"></div>
					<tr>
						<td class="two wide  center aligned ">
							姓名
						</td>
						<td class="three wide  center aligned ">
								${student.stuName}
						</td>
						<td class="two wide  center aligned ">
							学号
						</td>
						<td class="three wide  center aligned ">
                            ${student.stuId}
						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							密码
						</td>
						<td class="three wide  center aligned ">

                            ${student.password}

						</td>
						<td class="two wide  center aligned ">
							专业
						</td>
						<td class="three wide  center aligned ">
                            ${student.major}

						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							QQ
						</td>
						<td class="three wide  center aligned ">
                            ${student.QQ}
						</td>
						<td class="two wide  center aligned ">
							邮件地址
						</td>
						<td class="three wide  center aligned ">
                            ${student.email}
						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							性别
						</td>
						<td class="three wide  center aligned ">
                            ${student.gender}
						</td>
						<td class="two wide  center aligned ">
							电话号码
						</td>
						<td class="three wide  center aligned ">
                            ${student.tel}
						</td>
					</tr>

					</tbody>
				</table>
			</form>
		</div>
		<div class="three wide column"></div>
	</div>


</@override>
<@extends name="student_layout.ftl"></@extends>