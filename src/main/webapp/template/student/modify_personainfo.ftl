<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">修改个人信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<form class="ui form">
				<table class="ui celled striped table fluid">
					<tbody>
					<div class="ui hidden divider"></div>
					<tr>
						<td class="two wide  center aligned ">
							姓名
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="text" name="stuName" value="${student.stuName}">
							</div>

						</td>
						<td class="two wide  center aligned ">
							学号
						</td>
						<td class="three wide  center aligned ">
							<div class="field disabled">
								<input type="text" name="stuId" value="${student.stuId}">
							</div>
						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							密码
						</td>
						<td class="three wide  center aligned ">

							<div class="field">
								<input type="text" name="password" value="${student.password}">
							</div>

						</td>
						<td class="two wide  center aligned ">
							专业
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="text" name="major" value="${student.major}">
							</div>

						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							QQ
						</td>
						<td class="three wide  center aligned ">

							<div class="field">
								<input type="text" name="QQ" value="${student.QQ}">
							</div>

						</td>
						<td class="two wide  center aligned ">
							邮件
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="text" name="email" value="${student.email}">
							</div>

						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							电话号码
						</td>
						<td class="three wide  center aligned ">

							<div class="field">
								<input type="text" name="tel" value="${student.tel}">
							</div>

						</td>
						<td class="two wide  center aligned ">
							性别
						</td>
						<td class="three wide  center aligned ">
							<select class="ui dropdown" name="gender">
								<option value="${student.gender}">选择性别</option>
								<option value="男">男</option>
								<option value="女">女</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="ui error message"></div>
						</td>
						<td>
							<div class="ui fluid large teal submit button ">保存</div>
						</td>
					</tr>


					</tbody>
				</table>
			</form>
		</div>
		<div class="three wide column"></div>
	</div>

	<script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                stuName: {
                    rules: [{
                        type: 'empty',
                        prompt: 'Name不能为空'
                    }]
                },
                stuID: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: '学号不能为空'
                        }]
                },
                password: {
                    rules: [{
                        type: 'empty',
                        prompt: '密码不能为空'
                    }]
                },
                major: {
                    rules: [{
                        type: 'empty',
                        prompt: 'type不能为空'
                    }]
                }
                ,
                QQ: {
                    rules: [{
                        type: 'empty',
                        prompt: 'QQ不能为空'
                    }]
                },
                tel: {
                    rules: [{
                        type: 'empty',
                        prompt: '电话不能为空'
                    }]
                }
                ,
                email: {
                    rules: [{
                        type: 'empty',
                        prompt: 'emial不能为空'
                    }]
                },
                gender: {
                    rules: [{
                        type: 'empty',
                        prompt: 'gender不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/student/SavePersonInfo',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/student/personainfo'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

	</script>
</@override>
<@extends name="student_layout.ftl"></@extends>