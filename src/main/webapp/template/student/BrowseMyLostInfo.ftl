<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">my丢失信息</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="two wide column"></div>
		<div class="twelve wide column">
		<#if ! lostthings.isEmpty() >
				<table class="ui celled striped table fluid">

				<thead>
				<tr>
					<th class="center aligned">序号</th>
					<th class="center aligned">物品名字</th>
					<th class="center aligned">丢失地点</th>
					<th class="center aligned">丢失时间</th>
					<th class="center aligned">物品类型</th>
					<th class="center aligned">发布时间</th>
					<th class="center aligned">状态</th>
					<th class="center aligned">操作</th>
				</tr>
				</thead>
					<tbody>
					<#list lostthings as lostthing>
					<tr>
						<td class="center aligned">${lostthing.id}</td>
						<td class="center aligned">${lostthing.thingsName}</td>
						<td class="center aligned">${lostthing.lostPlace}</td>
						<td class="center aligned">${lostthing.lostTime}</td>
						<td class="center aligned">${lostthing.type}</td>
						<td class="center aligned">${lostthing.publishTime}</td>
						<#if lostthing.status==1 >
						    <td class="center aligned">已找到</td>
							<td class="center aligned disabled">
								<a href="${base}/student/DelelteLostInfo/${lostthing.id} "class="button ui ">删除</a>
								<a href="${base}/student/ModifyLostInfo/${lostthing.id} "class="button ui ">修改</a>
								<a href="${base}/student/UploadLostInfoImg/${lostthing.id} "class="button ui ">上传图片</a>
								<a href="${base}/student/PassLostInfo/${lostthing.id} "class="button ui ">确认找到</a>
							</td>
						<#else>
						 <td class="center aligned">未找到</td>
							<td class="center aligned">
								<a href="${base}/student/DelelteLostInfo/${lostthing.id} "class="button ui red">删除</a>
								<a href="${base}/student/ModifyLostInfo/${lostthing.id} "class="button ui blue">修改</a>
								<a href="${base}/student/UploadLostInfoImg/${lostthing.id} "class="button ui green">上传图片</a>
								<a href="${base}/student/PassLostInfo/${lostthing.id} "class="button ui teal">确认找到</a>
							</td>
						</#if>


					</tr>
                    </#list>
					</tbody>
				</table>
			<#else >
				<div class="ui negative message">
					<i class="close icon"></i>
					<div class="header">
						没有信息！！！
					</div>
					</div>
        </#if>
		</div>
		<div class=" two wide column">
		</div>
	</div>

</@override>
<@extends name="student_layout.ftl"></@extends>