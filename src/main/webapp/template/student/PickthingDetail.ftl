<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">个人信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<table class="ui celled striped table fluid">
				<tbody>
				<div class="ui hidden divider"></div>
				<tr>
					<td class="two wide  center aligned ">
						物品名字
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.thingsName!''}
					</td>
					<td class="two wide  center aligned ">
						物品类型
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.type!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						被捡到的地点
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.pickPlace!''}
					</td>
					<td class="two wide  center aligned ">
						被捡到时间
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.pickTime!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						保存地点
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.storagePlace!''}
					</td>
					<td class="two wide  center aligned ">
						发布时间
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.publishTime!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						发布者
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.stuName!''}
					</td>
					<td class="two wide  center aligned ">
						发布者电话
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.tel!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						发布者邮箱
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.email!''}
					</td>
					<td class="two wide  center aligned ">
						发布者QQ
					</td>
					<td class="three wide  center aligned ">
                        ${pickthing.QQ!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						物品图片
					</td>
					<td class="three wide  center aligned ">
						<img class="ui medium rounded image center aligned" src="${base}${pickthing.thingsImg!''}">
					</td>
					<td class="two wide  center aligned " >
						物品细节描述
					</td>
					<td class="three wide  center aligned " >
                        ${pickthing.thingsDetail!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						操作
					</td>
					<td class="three wide  center aligned " colspan="3">
						<#if pickthing.status==0>
							<a href="${base}/student/Claim/${pickthing.id}" class="ui button green">认领物品</a>
							<#else>
								<a href="${base}/student/000/${pickthing.id}" class="ui button green disabled">物品已被认领</a>
                        </#if>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
		<div class="three wide column"></div>
	</div>


</@override>
<@extends name="student_layout.ftl"></@extends>