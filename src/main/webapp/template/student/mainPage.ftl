<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">个人信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="two wide column"></div>
		<div class="twelve wide column">
			<table class="ui celled striped table fluid">
				<tbody>
				<tr>
					<td class="eight wide  center aligned ">
                        <#if flag==1>
							<div class="ui top attached tabular menu">
								<a class="item active" href="${base}/student/mainPage">
									最新招领信息
								</a>
								<a class="item " href="${base}/student/mainPageForLostInfo">
									最新寻物信息
								</a>
							</div>
							<div class="ui bottom attached segment">
								<table class="ui small table">
									<tbody>
                                    <#list pickthings as pickthing>
										<tr>
											<td class="one wide center aligned"><i class="volume up icon"></i></td>
											<td class="five wide">
												<a href="${base}/student/PickthingDetail/${pickthing.id}">
													<b>${pickthing.thingsName!''}</b>被捡到啦！，快来领取呀！！！
                                                    <#if pickthing.status==1>
														<font color="red"><b><i>成功贴！</i></b></font>
                                                    </#if>
												</a>
											</td>
											<td class="two wide center aligned">
                                                ${pickthing.publishTime}
											</td>
										</tr>
                                    </#list>
									</tbody>

								</table>
							</div>
                        <#else >
	                        <div class="ui top attached tabular menu">
		                        <a class="item " href="${base}/student/mainPage">
			                        最新招领信息
		                        </a>
		                        <a class="item active " href="${base}/student/mainPageForLostInfo">
			                        最新寻物信息
		                        </a>
	                        </div>
	                        <div class="ui bottom attached segment">
		                        <table class="ui small table">
			                        <tbody>
                                    <#list lostthings as lostthing>
				                        <tr>
					                        <td class="one wide center aligned"><i class="volume up icon"></i></td>
					                        <td class="five wide">
						                        <a href="${base}/student/LostthingDetail/${lostthing.id}">
							                        <b>${lostthing.thingsName!''}</b>丢啦！，是谁捡到了呢？？？
                                                    <#if lostthing.status==1>
								                        <font color="red"><b><i>成功贴！</i></b></font>
                                                    </#if>
						                        </a>
					                        </td>
					                        <td class="two wide center aligned">
                                                ${lostthing.publishTime}
					                        </td>
				                        </tr>
                                    </#list>
			                        </tbody>

		                        </table>
	                        </div>
                        </#if>
					</td>
					<td class="four wide   center aligned">
						<table class="ui celled striped table fluid">
							<thead>
							<th colspan="2" class="center aligned">
								XX大学失物招领平台  公告栏
							</th>
							</thead>
							<tbody>
							<tr>
								<td colspan="2" class="center aligned">
                                    ${notice.title!''}
								</td>
							</tr>
							<tr >
								<td colspan="2">
									<div class="ui text-area" style="height: 320px">
										${notice.content!''}
									</div>
								</td>
							</tr >
							<tr>
								<td class="center aligned">
									<a href="${base}/student/ReleasePickInfo" class="ui button blue">捡到东西啦</a>
								</td>
								<td class="center aligned">
									<a href="${base}/student/ReleaseLostInfo" class="ui button blue">丢失东西啦</a>
								</td>
							</tr>
							</tbody>
						</table>

					</td>
				</tr>

				</tbody>
			</table>
		</div>
		<div class="two wide column"></div>
	</div>


</@override>
<@extends name="student_layout.ftl"></@extends>