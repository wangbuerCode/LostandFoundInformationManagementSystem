<!doctype html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<script src="${base}/scripts/jquery.min.js"></script>
	<script src="${base}/scripts/semantic.min.js"></script>
	<link rel="stylesheet" href="${base}/styles/semantic.min.css"/>

	<title><@block name="title"></@block></title>

</head>
<body>
<img class="ui fluid image" src="${base}/template/images/99.jpg" style="height: 250px;width: 100%">

<div class="ui large  menu center aligned grey inverted " style="margin: 0">
	<div class="header item">
		xx大学
	</div>
	<a class=" item" href="${base}/student/mainPage">
		首页
	</a>
	<div class=" ui dropdown item">
		招领信息
		<div class="menu">
			<a class="item" href="${base}/student/BrowseMyPickInfo">查看个人发布</a>
			<a class="item" href="${base}/student/BrowseAllPickInfo">查看全部信息</a>
		</div>
	</div>

	<div class=" ui dropdown item">
		寻物信息
		<div class="menu">
			<a class="item" href="${base}/student/BrowseMyLostInfo">查看个人发布</a>
			<a class="item" href="${base}/student/BrowseAllLostInfo">查看全部信息</a>
		</div>
	</div>
	<a class=" item" href="${base}/student/BrowseAllExpressThanks">
		留言感谢墙
	</a>

	<div class=" ui dropdown item">
		发布信息
		<div class="menu">
			<a class="item" href="${base}/student/ReleaseThanks">发布感谢信息</a>
			<a class="item" href="${base}/student/ReleasePickInfo">发布招领信息</a>
			<a class="item" href="${base}/student/ReleaseLostInfo">发布寻物信息</a>
		</div>
	</div>
	<div class=" ui dropdown item">
		个人信息
		<div class="menu">
			<a class="item" href="${base}/student/personainfo">查看个人信息</a>
			<a class="item" href="${base}/student/modify_personainfo">修改个人信息</a>
		</div>
	</div>

	<div class="right menu">


        <#if (session.user)??>
			<div class="ui dropdown item">
				<i class="user icon"></i>
				<div class="text"> 欢迎 ${session.user.stuName} 同学</div>
				<i class="icon dropdown"></i>
				<div class="menu">
					<a href="${base}/student/logout" class="item">退出</a>
				</div>
			</div>
        <#else>

			<div class="ui buttons">
				<a class="ui button" href="${base}/login">登陆</a>
				<div class="or"></div>
				<a class="ui positive button" href="${base}/register">注册</a>
			</div>
        </#if>
	</div>
</div>


<@block name="content">

</@block>

<#--<div class="ui footer menu bottom fixed  ">-->
<#--	<div class="ui text container" style="font-size:20px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宁夏大学-->
<#--		信息工程学院-->
<#--	</div>-->
<#--</div>-->
</body>

<script>
    $('.ui.dropdown').dropdown();
</script>

</html>