<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">个人信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<table class="ui celled striped table fluid">
				<tbody>
				<div class="ui hidden divider"></div>
				<tr>
					<td class="two wide  center aligned ">
						物品名字
					</td>
					<td class="three wide  center aligned ">
                        ${lostthing.thingsName!''}
					</td>
					<td class="two wide  center aligned ">
						物品类型
					</td>
					<td class="three wide  center aligned ">
                        ${lostthing.type!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						丢失地点
					</td>
					<td class="three wide  center aligned ">
                        ${lostthing.lostPlace!''}
					</td>
					<td class="two wide  center aligned ">
						丢失时间
					</td>
					<td class="three wide  center aligned ">
                        ${lostthing.lostTime!''}
					</td>
				</tr>

				<tr>
					<td class="two wide  center aligned ">
						发布者
					</td>
					<td class="three wide  center aligned ">
                        ${lostthing.stuName!''}
					</td>
					<td class="two wide  center aligned ">
						发布者电话
					</td>
					<td class="three wide  center aligned ">
                        ${lostthing.tel!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						发布者邮箱
					</td>
					<td class="three wide  center aligned ">
                        ${lostthing.email!''}
					</td>
					<td class="two wide  center aligned ">
						发布者QQ
					</td>
					<td class="three wide  center aligned ">
                        ${lostthing.QQ!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						物品图片
					</td>
					<td class="three wide  center aligned ">
						<img class="ui medium rounded image center aligned" src="${base}${lostthing.thingsImg!''}">
					</td>
					<td class="two wide  center aligned " >
						物品细节描述
					</td>
					<td class="three wide  center aligned " >
                        ${lostthing.thingsDetail!''}
					</td>
				</tr>
				<tr>
					<td class="two wide  center aligned ">
						状态
					</td>
					<td class="three wide  center aligned " colspan="3">
                        <#if lostthing.status==0>
							<a href="${base}/student/Claim/${lostthing.id}" class="ui button green disabled">物品未找到</a>
                        <#else>
							<a href="${base}/student/000/${lostthing.id}" class="ui button green disabled">物品已找到</a>
                        </#if>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
		<div class="three wide column"></div>
	</div>


</@override>
<@extends name="student_layout.ftl"></@extends>