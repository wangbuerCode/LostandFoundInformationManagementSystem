<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">丢失信息</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">


            <#if !  page.getList().isEmpty()  >

				<table class="ui red table equal width celled padded ">
					<thead>
					<tr>
						<th class=" center aligned  " colspan="5">
							按条件显示
						</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							<form class="ui fluid form">
								<table class="ui celled table">
									<tbody>
									<tr>

										<td class="ten wide"></td>
										<td class="center aligned right">
											<div class="filed">
												<div class="ui labeled input ">
													<div class="ui label">
														物品类型
													</div>
													<select class="ui dropdown" name="thingsType">
														<option value="">选择类型</option>
                                                        <#list types as type>
															<option value="${type.id}">${type.type}</option>
                                                        </#list>
													</select>
												</div>
											</div>
										</td>
										<td class="center aligned right">
											<a class="ui primary submit button">搜索</a><br>
										</td>
									</tr>
									<tr>
										<div class="ui error message"></div>
									</tr>

									</tbody>
								</table>
							</form>
						</td>
					</tr>

					</tbody>

				</table>

				<table class="ui celled striped table fluid">

					<thead>
					<tr>
						<th class="center aligned">序号</th>
						<th class="center aligned">物品名字</th>
						<th class="center aligned">丢失地点</th>
						<th class="center aligned">丢失时间</th>
						<th class="center aligned">物品类型</th>
						<th class="center aligned">发布时间</th>
						<th class="center aligned">发布人</th>
						<th class="center aligned">状态</th>
						<th class="center aligned">操作</th>
					</tr>
					</thead>
					<tbody>
                    <#list page.getList()  as lostthing>
						<tr>
							<td class="center aligned">${lostthing.id}</td>
							<td class="center aligned">${lostthing.thingsName}</td>
							<td class="center aligned">${lostthing.lostPlace}</td>
							<td class="center aligned">${lostthing.lostTime}</td>
							<td class="center aligned">${lostthing.type}</td>
							<td class="center aligned">${lostthing.publishTime}</td>
							<td class="center aligned">${lostthing.stuName}</td>
                            <#if lostthing.status == 1 >
								<td class="center aligned">已找到物品</td>
                            <#else>
								<td class="center aligned">未找到物品</td>
                            </#if>
							<td class="center aligned">

								<a href="${base}/student/LostthingDetail/${lostthing.id} "
								   class="button ui green ">查看详细信息</a>
							</td>
						</tr>
                    </#list>
					</tbody>
					<tfoot>
					<tr>
						<th colspan="9">
							<div class="ui right floated pagination menu">
                                <#if page.getPageNumber() !=1 >
									<a class="icon item" href="?page=${page.getPageNumber()-1}">
										<i class="left chevron icon"></i>
									</a>
                                </#if>
                                <#list  1..(page.getTotalPage()) as count >
									<a class="item">
                                        <#if page.getPageNumber()==count>
											<b>${count}</b>
                                        <#else>
                                            ${count}
                                        </#if>
									</a>
                                </#list>
                                <#if page.getPageNumber() != page.getTotalPage() >
									<a class="icon item" href="?page=${page.getPageNumber()+1}">
										<i class="right chevron icon"></i>
									</a>
                                </#if>
							</div>
						</th>
					</tr>
					</tfoot>
				</table>
            <#else >
				<div class="ui negative message">
					<i class="close icon"></i>
					<div class="header">
						没有信息！！！
					</div>
				</div>
            </#if>
		</div>
		<div class=" three wide column">
		</div>
	</div>


	<script>
        $('.ui.form').form({
            fields: {}
        }).api({
            method: 'POST',
            url: '${base}/student/queryLost',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    window.location.href = '${base}/student/query_Lostresult'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        });
	</script>
</@override>
<@extends name="student_layout.ftl"></@extends>