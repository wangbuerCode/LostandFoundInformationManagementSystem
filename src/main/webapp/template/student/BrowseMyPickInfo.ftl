<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">我的招领信息</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
            <#if ! pickthings.isEmpty() >
				<table class="ui celled striped table fluid">

					<thead>
					<tr>
						<th class="center aligned">序号</th>
						<th class="center aligned">物品名字</th>
						<th class="center aligned">捡到地点</th>
						<th class="center aligned">捡到时间</th>
						<th class="center aligned">物品类型</th>
						<th class="center aligned">发布时间</th>
						<th class="center aligned">状态</th>
						<th class="center aligned">操作</th>
					</tr>
					</thead>
					<tbody>
                    <#list pickthings as pickthing>
						<tr>
							<td class="center aligned">${pickthing.id}</td>
							<td class="center aligned">${pickthing.thingsName}</td>
							<td class="center aligned">${pickthing.pickPlace}</td>
							<td class="center aligned">${pickthing.pickTime}</td>
							<td class="center aligned">${pickthing.type}</td>
							<td class="center aligned">${pickthing.publishTime}</td>
                            <#if pickthing.status==1 >
								<td class="center aligned">已找到失主</td>
                            <#else>
								<td class="center aligned">未找到失主</td>
                            </#if>
							<td class="center aligned">
								<a href="${base}/student/DeleltePickInfo/${pickthing.id} "class="button ui red">删除</a>
								<a href="${base}/student/ModifyPickInfo/${pickthing.id} "class="button ui blue">修改</a>
								<a href="${base}/student/UploadPickInfoImg/${pickthing.id} "class="button ui green">上传图片</a>
								<#if pickthing.status==0 >
								<a href="${base}/student/FinishPickInfoImg/${pickthing.id} "class="button ui green">标记为完成</a>
									<#else >
										<a href="${base}/student/FinishPickInfoImg/${pickthing.id} "class="button ui green disabled">标记为完成</a>
                                </#if>
								<a href="${base}/student/PassPickInfo/${pickthing.id} "class="button ui teal">申请信息</a>
							</td>
						</tr>
                    </#list>
					</tbody>
				</table>
            <#else >
				<div class="ui negative message">
					<i class="close icon"></i>
					<div class="header">
						没有信息！！！
					</div>
				</div>
            </#if>
		</div>
		<div class=" three wide column">
		</div>
	</div>

</@override>
<@extends name="student_layout.ftl"></@extends>