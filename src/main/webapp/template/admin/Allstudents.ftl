<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">学生信息</@override>
<@override name="content">
	<td class="two wide "></td>
	<td class="twelve  wide ">
		<table class="ui red table equal width celled padded ">
			<thead>
			<tr>
				<th class=" center aligned  " colspan="5">
					搜索
				</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					<form class="ui fluid form">
						<table class="ui celled table">
							<tbody>
							<tr>
								<td>
									<div class="filed">
										<div class="ui labeled input">
											<div class="ui label">
												学号
											</div>
											<input type="text" placeholder="请输入学号" name="stuId">
										</div>
									</div>
								</td>
								<td>
									<div class="filed">
										<div class="ui labeled input ">
											<div class="ui label">
												姓名
											</div>
											<input type="text" placeholder="请输入姓名" name="stuName">
										</div>
									</div>
								</td>
								<td>
									<div class="filed">
										<div class="ui labeled input ">
											<div class="ui label">
												专业
											</div>
											<select class="ui dropdown" name="major">
												<option value="">专业</option>
												<option value="null">不限</option>
												<option value="计算机">计算机</option>
												<option value="软件工程">软件工程</option>
												<option value="网络工程">网络工程</option>

											</select>
										</div>
									</div>
								</td>
								<td>
									<a class="ui primary submit button">搜索</a><br>
								</td>
							</tr>
							<tr>
								<div class="ui error message"></div>
							</tr>

							</tbody>
						</table>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					<table class="ui celled table">
						<thead>
						<tr>
							<th class="center aligned">学号</th>
							<th class="center aligned">姓名</th>
							<th class="center aligned">专业</th>
							<th class="center aligned">QQ</th>
							<th class="center aligned">性别</th>
							<th class="center aligned">邮件</th>
							<th class="center aligned">电话</th>
							<th class="center aligned">操作</th>
						</tr>
						</thead>
						<tbody>
                        <#if  !  page.getList().isEmpty()  >
                            <#list   page.getList() as student>
								<tr>
									<td class="center aligned">
                                        ${student.stuId}
									</td>
									<td class="center aligned">
                                        ${student.stuName}
									</td>
									<td class="center aligned">
                                        ${student.major}
									</td>
									<td class="center aligned">
                                        ${student.QQ}
									</td>
									<td class="center aligned">
                                        ${student.gender}
									</td>
									<td class="center aligned">
                                        ${student.email}
									</td>
									<td class="center aligned">
                                        ${student.tel}
									</td>
									<td class="center aligned">
										<a href="${base}/admin/AdminEditStuInfo/${student.stuId}"
										   class="ui button primary">修改</a>

										<a href="${base}/admin/AdminDeleteStuInfo/${student.stuId}"
										   class="ui button red primary">删除</a>
									</td>
								</tr>

                            </#list>
                        <#else >

							<tr>
								<td class="fluid">
									<div class="ui negative massive message">
										<div class="header">
											未查询到相关信息
										</div>
									</div>
								</td>
							</tr>

                        </#if>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
			<tfoot>
			<tr>
				<th colspan="5">
					<div class="ui right floated pagination menu">
                        <#if page.getPageNumber() !=1 >
							<a class="icon item" href="?page=${page.getPageNumber()-1}">
								<i class="left chevron icon"></i>
							</a>
                        </#if>
                        <#list  1..(page.getTotalPage()) as count >
							<a class="item">
                                <#if page.getPageNumber()==count>
									<b>${count}</b>
                                <#else>
                                    ${count}
                                </#if>
							</a>
                        </#list>
                        <#if page.getPageNumber() != page.getTotalPage() >
							<a class="icon item" href="?page=${page.getPageNumber()+1}">
								<i class="right chevron icon"></i>
							</a>
                        </#if>
					</div>
				</th>
			</tr>
			</tfoot>
		</table>
	</td>
	<td class="two wide"></td>


	<script>
        $('.ui.form').form({
            fields: {}
        }).api({
            method: 'POST',
            url: '${base}/admin/admin_query_stu',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    window.location.href = '${base}/admin/admin_query_stu_result'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        });
	</script>

</@override>
<@extends name="admin_layout.ftl"></@extends>