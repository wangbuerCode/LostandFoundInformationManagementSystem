<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">管理丢失信息</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">

            <#if !  page.getList().isEmpty()  >

				<table class="ui celled striped table fluid">

					<thead>
					<tr>
						<th class="center aligned">序号</th>
						<th class="center aligned">标题</th>
						<th class="center aligned">内容</th>
						<th class="center aligned">发布时间</th>
					</tr>
					</thead>
					<tbody>
                    <#list page.getList()  as notice>
						<tr>
							<td class="center aligned">${notice.id}</td>
							<td class="center aligned">${notice.title}</td>
							<td class="center aligned">${notice.content}</td>
							<td class="center aligned">${notice.publishTime}</td>
						</tr>
                    </#list>
					</tbody>
					<tfoot>
					<tr>
						<th colspan="9">
							<div class="ui right floated pagination menu">
                                <#if page.getPageNumber() !=1 >
									<a class="icon item" href="?page=${page.getPageNumber()-1}">
										<i class="left chevron icon"></i>
									</a>
                                </#if>
                                <#list  1..(page.getTotalPage()) as count >
									<a class="item">
                                        <#if page.getPageNumber()==count>
											<b>${count}</b>
                                        <#else>
                                            ${count}
                                        </#if>
									</a>
                                </#list>
                                <#if page.getPageNumber() != page.getTotalPage() >
									<a class="icon item" href="?page=${page.getPageNumber()+1}">
										<i class="right chevron icon"></i>
									</a>
                                </#if>
							</div>
						</th>
					</tr>
					</tfoot>
				</table>
            <#else >
				<div class="ui negative message">
					<i class="close icon"></i>
					<div class="header">
						没有信息！！！
					</div>
				</div>
            </#if>
		</div>
		<div class=" three wide column">
		</div>
	</div>



</@override>
<@extends name="admin_layout.ftl"></@extends>