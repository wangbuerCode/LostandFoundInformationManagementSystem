<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">发布公告</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">

			<form class="ui form">
				<div class="ui raised very padded text container segment">
					<div class=" field">
						<label>标题：</label>
					<h2 class="ui header">
						<input type="text" name="title" >
					</h2>
					</div>
					<div class=" field">
						<label>内容：</label>
					<p>
						<textarea name="content"></textarea>
					</p>
						<div class="ui error message"></div>
						<div class="ui fluid large teal submit button ">发布</div>
					</div>
				</div>
			</form>
		</div>
		<div class=" three wide column">
		</div>
	</div>

	<script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                title: {
                    rules: [{
                        type: 'empty',
                        prompt: 'title'
                    }]
                },
                comtent: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'comtent不能为空'
                        }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/admin/DoReleaseNotice',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/admin/BrowseNoticeInfo'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

	</script>

</@override>
<@extends name="admin_layout.ftl"></@extends>