<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">修改招领信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<form class="ui form">
				<table class="ui celled striped table fluid">
					<tbody>
					<div class="ui hidden divider"></div>

					<tr>
						<td class="two wide  center aligned ">
							序号
						</td>
						<td class="three wide  center aligned ">
							<div class="field disabled">
								<input type="text" name="id" value="${pickthing.id!''}" >
							</div>
						</td>

						<td class="two wide  center aligned ">
							发布者学号
						</td>
						<td class="three wide  center aligned " >
							<div class="field disabled">
								<input type="text" name="thingsDetail" value="${pickthing.stuId!''}" placeholder="${pickthing.stuId!''}">
							</div>
						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							物品名字
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="text" name="thingsName" value=" ${pickthing.thingsName!''}" placeholder=" ${pickthing.thingsName!''}">
							</div>

						</td>
						<td class="two wide  center aligned ">
							物品类型
						</td>
						<td class="three wide  center aligned ">
							<select class="ui dropdown" name="thingsType">
								<option value="${pickthing.type!''}">选择类型</option>
                                <#list types as type>
									<option value="${type.id}">${type.type}</option>
                                </#list>
							</select>
						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							捡到地点
						</td>
						<td class="three wide  center aligned ">

							<div class="field">
								<input type="text" name="pickPlace" value="${pickthing.pickPlace!''}" placeholder=" ${pickthing.picktPlace!''}">
							</div>

						</td>
						<td class="two wide  center aligned ">
							捡到时间
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="date" name="pickTime" value="${pickthing.pickTime!''}">
							</div>

						</td>
					</tr>

					<tr>
						<td class="two wide  center aligned ">
							物品细节描述
						</td>
						<td class="three wide  center aligned " >
							<div class="field">
								<input type="text" name="storagePlace" value="${pickthing.storagePlace!''}" placeholder="${pickthing.storagePlace!''}">
							</div>
						</td>

						<td class="two wide  center aligned ">
							物品细节描述
						</td>
						<td class="three wide  center aligned " >
							<div class="field">
								<input type="text" name="thingsDetail" value="${pickthing.thingsDetail!''}" placeholder="${pickthing.thingsDetail!''}">
							</div>
						</td>
					</tr>
					<tr>

						<td colspan="3">
							<div class="ui error message"></div>
						</td>
						<td>
							<div class="ui fluid large teal submit button ">保存</div>
						</td>
					</tr>

					</tbody>
				</table>
			</form>
		</div>
		<div class="three wide column"></div>
	</div>

	<script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                thingsName: {
                    rules: [{
                        type: 'empty',
                        prompt: 'thingsName不能为空'
                    }]
                },
                thingsDetail: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: '描述不能为空'
                        }]
                },
                pickPlace: {
                    rules: [{
                        type: 'empty',
                        prompt: '捡到地点不能为空'
                    }]
                },
                thingsType: {
                    rules: [{
                        type: 'empty',
                        prompt: 'type不能为空'
                    }]
                }
                ,
                pickTime: {
                    rules: [{
                        type: 'empty',
                        prompt: '捡到时间不能为空'
                    }]
                }
                ,
                storagePlace: {
                    rules: [{
                        type: 'empty',
                        prompt: '存放位置不能为空'
                    }]
                }

            }
        }).api({
            method: 'POST',
            url: '${base}/admin/adminDoModifyPickInfo',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/admin/ManagePickInfo'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

	</script>
</@override>
<@extends name="admin_layout.ftl"></@extends>