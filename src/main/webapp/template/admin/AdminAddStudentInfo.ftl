<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">学生信息</@override>
<@override name="content">


	<div class="ui equal width center aligned padded grid">

		<div class="row">
			<div class=" column three wide">

			</div>
			<div class=" column ten wide">
				<table class="ui table celled padded  fluid ">
					<tbody>
					<tr>
						<td class="ten wide">
							<table class="ui red table">
								<thead>
								<tr>
									<th>学生注册</th>
								</tr>
								</thead>

								<tbody>
								<tr>
									<td>
										<form class="ui form" id="login-form">
											<table class="ui  table      ">
												<tbody>
												<tr>
													<td class="center aligned two wide">
														<label >学号</label>
													</td>
													<td class="eight wide">
														<div class="field ">
															<input type="text" name="stuId" placeholder="请输入学号" >
														</div>
													</td>
													<td class="six wide">
														<label>这里是提示信息，学号要求</label>
													</td>
												</tr>

												<tr>
													<td class="center aligned two wide">
														<label >姓名</label>
													</td>
													<td class="eight wide">
														<div class="field ">
															<input type="text" name="stuName" placeholder="请输入姓名" >
														</div>
													</td>
													<td class="six wide">
														<label>这里是提示信息</label>
													</td>
												</tr>

												<tr>
													<td class="center aligned two wide">
														<label >密码</label>
													</td>
													<td class="eight wide">
														<div class="field ">
															<input type="text" name="password" placeholder="请输入密码" >
														</div>
													</td>
													<td class="six wide">
														<label>这里是提示信息</label>
													</td>
												</tr>

												<tr>
													<td class="center aligned two wide">
														<label >重输密码</label>
													</td>
													<td class="eight wide">
														<div class="field ">
															<input type="text" name="password1" placeholder="请再次输入密码" >
														</div>
													</td>
													<td class="six wide">
														<label>这里是提示信息</label>
													</td>
												</tr>

												<tr>
													<td class="center aligned two wide">
														<label >专业</label>
													</td>
													<td class="eight wide">
														<div class="field ">
															<input type="text" name="major" placeholder="请输入专业" >
														</div>
													</td>
													<td class="six wide">
														<label>这里是提示信息</label>
													</td>
												</tr>

												<tr>
													<td class="center aligned two wide">
														<label >性别</label>
													</td>
													<td class="eight wide">
														<div class="field">
															<select class="ui dropdown " name="gender">
																<option value="">选择性别</option>
																<option value="男">男</option>
																<option value="女">女</option>
															</select>
														</div>
													</td>
													<td class="six wide">
														<label>这里是提示信息</label>
													</td>
												</tr>

												<tr>
													<td class="center aligned two wide">
														<label >电话</label>
													</td>
													<td class="eight wide">
														<div class="field ">
															<input type="text" name="tel" placeholder="请输入电话号码" >
														</div>
													</td>
													<td class="six wide">
														<label>这里是提示信息</label>
													</td>
												</tr>

												<tr>
													<td class="center aligned two wide">
														<label >邮件地址</label>
													</td>
													<td class="eight wide">
														<div class="field ">
															<input type="text" name="email" placeholder="请输入邮件地址" >
														</div>
													</td>
													<td class="six wide">
														<label>这里是提示信息</label>
													</td>
												</tr>

												<tr>
													<td class="center aligned two wide">
														<label >QQ号码</label>
													</td>
													<td class="eight wide">
														<div class="field ">
															<input type="text" name="QQ" placeholder="请输入QQ号码" >
														</div>
													</td>
													<td class="这里是提示信息 wide">
														<label>用户名</label>
													</td>
												</tr>

												<tr>
													<td colspan="1">
													</td>
													<td colspan="1">
														<button class="ui button primary fluid" type="submit">注册</button>
													</td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td>
														<div class="ui error message"></div>
													</td>
													<td></td>
												</tr>
												</tbody>
											</table>
										</form>
									</td>
								</tr>

								</tbody>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div class=" column three wide">

			</div>
		</div>




	</div>

	<div class="ui footer menu bottom fixed  ">

		<div class="ui text container" style="font-size:20px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宁夏大学
			信息工程学院
		</div>
	</div>
	</body>

	<script>
        $('.ui.form').form({
            fields:{
                stuId:{
                    rules:[{
                        type:'empty',
                        prompt:'用户名不能为空'
                    }]
                },
                stuName:{
                    rules:[{
                        type:'empty',
                        prompt:'姓名不能为空'
                    }]
                },
                password:{
                    rules:[
                        {
                            type   : 'empty',
                            prompt : '请输入密码'
                        },
                        {
                            type:'minLength[6]',
                            prompt:'密码至少包含6个字符'
                        }]
                },
                password1:{
                    rules:[{
                        type:'match[password]',
                        prompt:'密码不一致'
                    }]
                },
                major:{
                    rules:[{
                        type:'empty',
                        prompt:'专业不能为空'
                    }]
                },
                QQ:{
                    rules:[{
                        type:'empty',
                        prompt:'QQ不能为空'
                    }]
                },
                gender:{
                    rules:[{
                        type:'empty',
                        prompt:'性别不能为空'
                    }]
                },
                tel:{
                    rules:[{
                        type:'empty',
                        prompt:'电话不能为空'
                    }]
                },
                email:{
                    rules:[{
                        type:'empty',
                        prompt:'邮件地址不能为空'
                    }]
                }
            }
        }).api({
            method:'POST',
            url:'${base}/admin/Admin_Stu_Register',
            serializeForm:true,
            success:function (res) {
                if(res.success){
                    alert(res.message);
                    window.location.href='${base}/admin/BrowseAllStudentInfo'
                }else{
                    $('.ui.form').form('add errors',[res.message]);
                }
            }
        })

        $('.ui.radio.checkbox').checkbox();

        $('.box').sowingMap({
            count: 7,
            time: 3000
        });

        $('.ui.dropdown').dropdown();


	</script>

</@override>
<@extends name="admin_layout.ftl"></@extends>