<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">管理感谢信息</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="two wide column"></div>
		<div class="twelve wide column">


            <#if !  page.getList().isEmpty()  >
				<table class="ui celled striped table fluid">
					<thead>
					<tr>
						<th class="center aligned">序号</th>
						<th class="center aligned">标题</th>
						<th class="center aligned">内容</th>
						<th class="center aligned">留言者学号</th>
						<th class="center aligned">留言者姓名</th>
						<th class="center aligned">留言时间</th>
						<th class="center aligned">操作</th>
					</tr>
					</thead>
					<tbody>
                    <#list page.getList()  as expressthanks>
						<tr>
							<td class="center aligned">${expressthanks.id}</td>
							<td class="center aligned">${expressthanks.title}</td>
							<td class="center aligned">${expressthanks.content}</td>
							<td class="center aligned">${expressthanks.stuId}</td>
							<td class="center aligned">${expressthanks.stuName}</td>
							<td class="center aligned">${expressthanks.leaveTime}</td>
							<td class="center aligned">
									<a href="${base}/admin/AdminDeleteExpressthanksInfo/${expressthanks.id} "
									   class="button ui green ">删除</a>
							</td>
						</tr>
                    </#list>
					</tbody>
					<tfoot>
					<tr>
						<th colspan="9">
							<div class="ui right floated pagination menu">
                                <#if page.getPageNumber() !=1 >
									<a class="icon item" href="?page=${page.getPageNumber()-1}">
										<i class="left chevron icon"></i>
									</a>
                                </#if>
                                <#list  1..(page.getTotalPage()) as count >
									<a class="item">
                                        <#if page.getPageNumber()==count>
											<b>${count}</b>
                                        <#else>
                                            ${count}
                                        </#if>
									</a>
                                </#list>
                                <#if page.getPageNumber() != page.getTotalPage() >
									<a class="icon item" href="?page=${page.getPageNumber()+1}">
										<i class="right chevron icon"></i>
									</a>
                                </#if>
							</div>
						</th>
					</tr>
					</tfoot>
				</table>
            <#else >
				<div class="ui negative message">
					<i class="close icon"></i>
					<div class="header">
						没有信息！！！
					</div>
				</div>
            </#if>
		</div>
		<div class=" two wide column">
		</div>
	</div>


</@override>
<@extends name="admin_layout.ftl"></@extends>