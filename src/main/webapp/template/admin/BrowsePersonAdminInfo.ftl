<#-- @ftlvariable name="student" type="model.Student" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">个人信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
				<table class="ui celled striped table fluid">
					<tbody>
					<div class="ui hidden divider"></div>

					<tr>
						<td class="two wide  center aligned ">
							工号
						</td>
						<td class="three wide  center aligned ">
                            ${admin.id!''}
						</td>

						<td class="two wide  center aligned ">
							姓名
						</td>
						<td class="three wide  center aligned ">
                            ${admin.name!''}
						</td>
					</tr>

					<tr>
						<td class="two wide  center aligned ">
							密码
						</td>
						<td class="three wide  center aligned ">
                            ${admin.password!''}
						</td>
						<td class="two wide  center aligned ">
							电话
						</td>
						<td class="three wide  center aligned">
                            ${admin.tel!''}
						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							QQ
						</td>
						<td class="three wide  center aligned ">
                            ${admin.qq!''}

						</td>
						<td class="two wide  center aligned ">
							邮箱
						</td>
						<td class="three wide  center aligned ">
                            ${admin.email!''}
						</td>
					</tr>

					</tbody>
				</table>

		</div>
		<div class="three wide column"></div>
	</div>

</@override>
<@extends name="admin_layout.ftl"></@extends>