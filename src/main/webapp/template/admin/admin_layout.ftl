<!doctype html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<script src="${base}/scripts/jquery.min.js"></script>
	<script src="${base}/scripts/semantic.min.js"></script>
	<link rel="stylesheet" href="${base}/styles/semantic.min.css"/>

	<title><@block name="title"></@block></title>

</head>
<body>
<img class="ui fluid image" src="${base}/template/images/88.jpg" style="height: 250px;width: 100%">

<div class="ui large  menu center aligned grey inverted " style="margin: 0">
	<div class="header item">
		XX大学
	</div>
	<a class=" item" href="${base}/admin/mainPage">
		首页
	</a>
	<a class=" item" href="${base}/admin/ManagePickInfo">
		招领信息管理
	</a>
	<a class=" item" href="${base}/admin/ManageLostInfo">
		寻物信息管理
	</a>
	<a class=" item" href="${base}/admin/ManageThanksInfo">
		留言信息管理
	</a>
	<a class=" item" href="${base}/admin/ManageApplyInfo">
		申请信息管理
	</a>
	<div class=" ui dropdown item">
		物品类型管理
		<div class="menu">
			<a class="item" href="${base}/admin/AddTypeInfo">添加类型信息</a>
			<a class="item" href="${base}/admin/BrowseAllTypeInfo">查看类型信息</a>
		</div>
	</div>
	<div class=" ui dropdown item">
		学生信息管理
		<div class="menu">
			<a class="item" href="${base}/admin/AddStudentInfo">添加学生信息</a>
			<a class="item" href="${base}/admin/BrowseAllStudentInfo">查看学生信息</a>
		</div>
	</div>

	<div class=" ui dropdown item">
		管理员信息管理
		<div class="menu">
			<a class="item" href="${base}/admin/AddAdminInfo">添加管理员信息</a>
			<a class="item" href="${base}/admin/BrowseAllAdminInfo">查看管理员信息</a>
		</div>
	</div>
	<div class=" ui dropdown item">
		个人信息管理
		<div class="menu">
			<a class="item" href="${base}/admin/ModifyPersonInfo">修改个人信息</a>
			<a class="item" href="${base}/admin/BrowsePersonInfo">查看个人信息</a>
		</div>
	</div>

	<div class=" ui dropdown item">
		公告管理
		<div class="menu">
			<a class="item" href="${base}/admin/ReleaseNotice">发布公告</a>
			<a class="item" href="${base}/admin/BrowseNoticeInfo">查看公告</a>
		</div>
	</div>

	<div class="right menu">


        <#if (session.user)??>
			<div class="ui dropdown item">
				<i class="user icon"></i>
				<div class="text"> 欢迎 ${session.user.name} 管理员</div>
				<i class="icon dropdown"></i>
				<div class="menu">
					<a href="${base}/student/logout" class="item">退出</a>
				</div>
			</div>
        <#else>

			<div class="ui buttons">
				<a class="ui button" href="${base}/login">登陆</a>
				<div class="or"></div>
				<a class="ui positive button" href="${base}/register">注册</a>
			</div>
        </#if>
	</div>
</div>


<@block name="content">

</@block>

<#--<div class="ui footer menu bottom fixed  ">-->
<#--	<div class="ui text container" style="font-size:20px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宁夏大学-->
<#--		信息工程学院-->
<#--	</div>-->
<#--</div>-->
</body>

<script>
    $('.ui.dropdown').dropdown();
</script>

</html>