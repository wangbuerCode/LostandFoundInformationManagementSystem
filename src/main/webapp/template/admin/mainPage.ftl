<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">my丢失信息</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<div class="ui raised very padded text container segment">
				<h2 class="ui header">欢迎使用失物招领平台</h2>
				<p>1.此系统应包括后台数据库的设计和前台开发实现</p>
				<p>	2. 实现前台用户的注册登录、信息的浏览、寻物信息的发布以及物品认领</p>
				<p>3.实现后台用户信息的审核、失物认领的审核以及基本信息的管理功能，包括失物招领信息的发布、删改以及查询等操作</p>
				<p>	4.部署该系统至服务器端，实现校园失物招领管理系统的上线</p>
				<p>开发工具：Eclipse/IntelliJ IDEA</p>
			</div>
		</div>
		<div class=" three wide column">
		</div>
	</div>

</@override>
<@extends name="admin_layout.ftl"></@extends>