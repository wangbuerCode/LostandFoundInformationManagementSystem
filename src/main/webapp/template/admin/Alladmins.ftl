<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">管理员信息</@override>
<@override name="content">
	<td class="two wide "></td>
	<td class="twelve  wide ">
		<table class="ui red table equal width celled padded ">
			<tbody>
			<tr>
				<td>
					<table class="ui celled table">
						<thead>
						<tr>
							<th class="center aligned">工号</th>
							<th class="center aligned">姓名</th>
							<th class="center aligned">密码</th>
							<th class="center aligned">QQ</th>
							<th class="center aligned">邮件</th>
							<th class="center aligned">电话</th>
							<th class="center aligned">操作</th>
						</tr>
						</thead>
						<tbody>
                        <#if  !  page.getList().isEmpty()  >
                            <#list   page.getList() as admin>
								<tr>
									<td class="center aligned">
                                        ${admin.id}
									</td>
									<td class="center aligned">
                                        ${admin.name}
									</td>
									<td class="center aligned">
                                        ${admin.password}
									</td>
									<td class="center aligned">
                                        ${admin.qq}
									</td>
									<td class="center aligned">
                                        ${admin.email}
									</td>
									<td class="center aligned">
                                        ${admin.tel}
									</td>
									<td class="center aligned">
										<a href="${base}/admin/AdminEditAdminInfo/${admin.id}"
										   class="ui button primary">修改</a>

										<a href="${base}/admin/AdminDeleteAdminInfo/${admin.id}"
										   class="ui button red primary">删除</a>
									</td>
								</tr>

                            </#list>
                        <#else >

							<tr>
								<td class="fluid">
									<div class="ui negative massive message">
										<div class="header">
											未查询到相关信息
										</div>
									</div>
								</td>
							</tr>

                        </#if>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
			<tfoot>
			<tr>
				<th colspan="5">
					<div class="ui right floated pagination menu">
                        <#if page.getPageNumber() !=1 >
							<a class="icon item" href="?page=${page.getPageNumber()-1}">
								<i class="left chevron icon"></i>
							</a>
                        </#if>
                        <#list  1..(page.getTotalPage()) as count >
							<a class="item">
                                <#if page.getPageNumber()==count>
									<b>${count}</b>
                                <#else>
                                    ${count}
                                </#if>
							</a>
                        </#list>
                        <#if page.getPageNumber() != page.getTotalPage() >
							<a class="icon item" href="?page=${page.getPageNumber()+1}">
								<i class="right chevron icon"></i>
							</a>
                        </#if>
					</div>
				</th>
			</tr>
			</tfoot>
		</table>
	</td>
	<td class="two wide"></td>




</@override>
<@extends name="admin_layout.ftl"></@extends>