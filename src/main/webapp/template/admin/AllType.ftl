<#-- @ftlvariable name="types" type="java.util.List<model.Type>" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">查看物品类型信息</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">


            <#if !  types.isEmpty()  >
				<table class="ui celled striped table fluid">
					<thead>
					<tr>
						<th class="center aligned">序号</th>
						<th class="center aligned">物品类型</th>
						<th class="center aligned">操作</th>
					</tr>
					</thead>
					<tbody>
                    <#list types as type>
						<tr>
							<td class="center aligned">${type.id}</td>
							<td class="center aligned">${type.type}</td>
							<td class="center aligned">
								<a href="${base}/admin/AdminEditTypeInfo/${type.id} "
								   class="button ui green ">编辑</a>
							</td>
						</tr>
                    </#list>
					</tbody>

				</table>
            <#else >
				<div class="ui negative message">
					<i class="close icon"></i>
					<div class="header">
						没有信息！！！
					</div>
				</div>
            </#if>
		</div>
		<div class=" three wide column">
		</div>
	</div>


</@override>
<@extends name="admin_layout.ftl"></@extends>