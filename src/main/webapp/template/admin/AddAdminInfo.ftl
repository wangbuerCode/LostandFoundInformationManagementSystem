<#-- @ftlvariable name="student" type="model.Student" -->
<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">添加管理员信息页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<form class="ui form">
				<table class="ui celled striped table fluid">
					<tbody>
					<div class="ui hidden divider"></div>

					<tr>
						<td class="two wide  center aligned ">
							工号
						</td>
						<td class="three wide  center aligned ">
							<div class="field ">
								<input type="text" name="id" >
							</div>
						</td>

						<td class="two wide  center aligned ">
							姓名
						</td>
						<td class="three wide  center aligned ">
							<div class="field ">
								<input type="text" name="name">
							</div>
						</td>
					</tr>

					<tr>
						<td class="two wide  center aligned ">
							密码
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="text" name="password">
							</div>

						</td>
						<td class="two wide  center aligned ">
							电话
						</td>
						<td class="three wide  center aligned">
							<div class="field">
								<input type="text" name="tel" >
							</div>
						</td>
					</tr>
					<tr>
						<td class="two wide  center aligned ">
							QQ
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="text" name="qq" >
							</div>

						</td>
						<td class="two wide  center aligned ">
							邮箱
						</td>
						<td class="three wide  center aligned ">
							<div class="field">
								<input type="text" name="email" >
							</div>

						</td>
					</tr>

					<tr>
						<td>

						</td>
						<td colspan="2">
							<div class="ui error message"></div>
						</td>
						<td>
							<div class="ui fluid large teal submit button ">保存</div>
						</td>
					</tr>

					</tbody>
				</table>
			</form>
		</div>
		<div class="three wide column"></div>
	</div>

	<script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                name: {
                    rules: [{
                        type: 'empty',
                        prompt: 'stuName'
                    }]
                },
                password: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'password不能为空'
                        }]
                },
                qq: {
                    rules: [{
                        type: 'empty',
                        prompt: 'QQ不能为空'
                    }]
                },
                tel: {
                    rules: [{
                        type: 'empty',
                        prompt: ' tel不能为空'
                    }]
                }
                ,
                eamil: {
                    rules: [{
                        type: 'empty',
                        prompt: 'eamil不能为空'
                    }]
                }
                ,
                id: {
                    rules: [{
                        type: 'empty',
                        prompt: '工号不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/admin/adminDoAddAdminInfo',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/admin/BrowseAllAdminInfo'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

	</script>
</@override>
<@extends name="admin_layout.ftl"></@extends>