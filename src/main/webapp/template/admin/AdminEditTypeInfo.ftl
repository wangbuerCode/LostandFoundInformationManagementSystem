<#-- @ftlvariable name="pickthing" type="model.Pickthings" -->
<#-- @ftlvariable name="lostthing" type="model.Lostthings" -->
<#-- @ftlvariable name="pickthings" type="java.util.List<model.Pickthings>" -->
<#-- @ftlvariable name="lostthings" type="java.util.List<model.Lostthings>" -->
<@override name="title">修改物品类型页</@override>
<@override name="content">

	<div class="ui hidden divider"></div>

	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<form class="ui form">
				<table class="ui celled striped table fluid">
					<tbody>
					<div class="ui hidden divider"></div>

					<tr>
						<td class="two wide  center aligned disabled ">
							序号
						</td>
						<td class="three wide  center aligned ">
							<div class="field disabled">
								<input type="text" name="id" value="${type.id!''}" >
							</div>
						</td>

						<td class="two wide  center aligned ">
							物品类型
						</td>
						<td class="three wide  center aligned " >
							<div class="field ">
								<input type="text" name="type" value="${type.type!''}" placeholder="${type.type!''}">
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="ui error message"></div>
						</td>
						<td>
							<div class="ui fluid large teal submit button ">保存</div>
						</td>
					</tr>

					</tbody>
				</table>
			</form>
		</div>
		<div class="three wide column"></div>
	</div>

	<script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                type: {
                    rules: [{
                        type: 'empty',
                        prompt: 'type'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/admin/adminDoEditTypeInfo',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/admin/BrowseAllTypeInfo'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

	</script>
</@override>
<@extends name="admin_layout.ftl"></@extends>