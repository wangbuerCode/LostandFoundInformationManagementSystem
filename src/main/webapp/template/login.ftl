<!doctype html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<script src="${base}/scripts/jquery.min.js"></script>
	<script src="${base}/scripts/semantic.min.js"></script>
	<link rel="stylesheet" href="${base}/styles/semantic.min.css"/>
	<title>登录</title>

	<style type="text/css">
		body {
			background-image: url("../template/image/bg17.jpeg");
			/*background-size:cover;*/
			background-size :100% 100%;

		}
		body > .grid {
			height: 100%;
		}
		.image {
			margin-top: -100px;
		}
		.column {
			max-width: 450px;
		}
		.mybox{
			position: relative;
		}
		.mybox span{
			position:absolute;
			display: block;
			height: 34px;
			right:2px;
			top:2px;
			line-height: 34px;
		}
		.content{
			color: #000000

		}
	</style>

</head>
<body>


<div class="ui middle aligned center aligned grid">
	<div class="column">
<#--		<img src="${base}/template/image/logo1.png" class="image">-->
		<h2 class="ui teal header">
			<div class="content" >
				欢迎登陆失物招领平台
			</div>
		</h2>
		<form class="ui large form"  id="login-form">
			<div class="ui stacked segment">
				<div class="field">
					<div class="ui left icon input">
						<i class="user icon"></i>
						<input type="text" name="username" placeholder="Account">
					</div>
				</div>
				<div class="field">
					<div class="ui left icon input">
						<i class="lock icon"></i>
						<input type="password" name="password" placeholder="Password">
					</div>
				</div>

				<div class="inline fields">
					<label for="usertype">选择用户类型   </label>
					<div class="field">
						<div class="ui radio checkbox">
							<input type="radio" name="usertype" id="" value="1">
							<label for="">  学生</label>
						</div>
					</div>
					<div class="field">
						<div class="ui radio checkbox">
							<input type="radio" name="usertype" id="" value="2">
							<label for="">  管理员</label>
						</div>
					</div>
				</div>

				<div class="ui error message"></div>

				<div class="ui large fluid buttons">
					<div class="ui  large teal submit button left">登录</div>
					<div class="or"></div>
					<a class="ui large  teal  button right " href="${base}/register"> 注册</a>
				</div>

			</div>
		</form>

	</div>
</div>

</body>
<script>
    $('.ui.form').form({
        fields:{
            username :{
                identifier:'username',
                rules:[
                    {
                        type:'empty',prompt:'用户名不能为空'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [{
                    type: 'empty', prompt: '密码不能为空'
                }]
            },
            usertype: {
                identifier: 'usertype',
                rules: [{
                    type: 'checked', prompt: '用户类型不能为空'
                }]
            }
        }
    }).api({
        method:'POST',
        url:'${base}/loginCheck',
        serializeForm:true,
        success:function (res) {
            if (res.success) {
                if (res.flag == 1) {
                    window.location.href = '${base}/student'
                } else if (res.flag == 2) {
                    window.location.href = '${base}/admin'
                }
            } else {
                $('.ui.form').form('add errors', [res.message]);
            }
        }

    });

</script>

</html>